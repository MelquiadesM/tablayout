package com.mel.tablayout;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import androidx.appcompat.widget.AppCompatTextView;

public class CustomTabLayout extends TabLayout {
    public CustomTabLayout(Context context) {
        super(context);
    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setTitles(ArrayList<String> titles) {
        for (int i = 0; i < titles.size(); i++) {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.tab_item, null, false);
            ((AppCompatTextView) v.findViewById(R.id.tvTitleTab)).setText(titles.get(i));
            ((AppCompatTextView) v.findViewById(R.id.tvTitleTab)).setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            Tab tab = getTabAt(i);
            if (tab != null) {
                tab.setCustomView(v);
            }
        }
    }

    public void setTabError(int position) {
        Tab tab = getTabAt(position);
        if (tab != null) {
            View view = tab.getCustomView();
            if (view != null) {
                ((AppCompatTextView) view.findViewById(R.id.tvTitleTab)).setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_error), null);
            }
        }
    }

    public void clearTabError(int position) {
        Tab tab = getTabAt(position);
        if (tab != null) {
            View view = tab.getCustomView();
            if (view != null) {
                ((AppCompatTextView) view.findViewById(R.id.tvTitleTab)).setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            }
        }
    }

    public void clearAllErrors() {
        for (int i = 0; i < getTabCount(); i++) {
            clearTabError(i);
        }
    }

}
